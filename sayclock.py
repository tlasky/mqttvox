import time
from datetime import datetime
from vox import MqttVoxClient

vox = MqttVoxClient()
announced = None
vox.say('Voice clocks are activated.')
while True:
    try:
        now = datetime.now()
        if 9 <= now.hour <= 22:
            if announced is None or (not announced and now.minute % 15 == 0):
                text = f'The time is {now.hour} hours'
                if now.minute != 0:
                    text += f' and {now.minute} minutes'
                text += '.'
                print(text)
                vox.play('fvox/bell.wav')
                vox.say(text)
                announced = True
            elif announced and now.minute % 15 != 0:
                announced = False
            time.sleep(0.1)
        else:
            time.sleep(1)
    except KeyboardInterrupt:
        break
vox.say('Voice clocks are deactivated.')
vox.close()
