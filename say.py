from vox import MqttVoxClient

vox = MqttVoxClient()
while True:
    try:
        text = input('Message: ')
        vox.play('misc/talk.wav')
        vox.say(text)
    except KeyboardInterrupt:
        break
vox.close()
