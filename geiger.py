import time
import random
from vox import MqttVoxClient

sounds = [
    f'player/geiger{i}.wav'
    for i in range(1, 7)
]

vox = MqttVoxClient()
while True:
    try:
        vox.play(random.choice(sounds))
        time.sleep(random.uniform(0.1, 1))
    except KeyboardInterrupt:
        break
vox.close()
