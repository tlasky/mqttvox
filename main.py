import os
from vox import MqttVox

if __name__ == '__main__':
    vox = MqttVox(
        sound_path=os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            'sound'
        )
    )
    try:
        vox.say('Voice system activated.')
        vox.run()
    except KeyboardInterrupt:
        vox.stop()
