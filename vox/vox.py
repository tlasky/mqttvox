import io
import os
import time
import paho.mqtt.client as mqtt
from typing import *
from gtts import gTTS
from queue import Queue
from threading import Thread
from .player import play, AudioSegment


class MqttVoxBase:
    default_mqtt_address = ('mqtt.eclipse.org', 1883)
    default_vox_topic = '/tlasky/vox'
    default_tts_topic = '/tlasky/tts'

    def __init__(self, mqtt_address: Tuple[str, int], vox_topic: str, tts_topic: str):
        self.mqtt_address = mqtt_address
        self.mqtt = mqtt.Client()
        self.vox_topic = vox_topic
        self.tts_topic = tts_topic

    def _connect(self):
        self.mqtt.connect(*self.mqtt_address, 60)


class MqttVox(MqttVoxBase):
    def __init__(self, mqtt_address: Tuple[str, int] = MqttVoxBase.default_mqtt_address, sound_path: str = None,
                 vox_topic: str = MqttVoxBase.default_vox_topic, tts_topic: str = MqttVoxBase.default_tts_topic):
        assert sound_path, 'Please specify your sound path.'
        super().__init__(mqtt_address, vox_topic, tts_topic)
        self.mqtt.on_connect = self._mqtt_on_connect
        self.mqtt.on_message = self._mqtt_on_message
        self._connect()

        self.sound_queue: Queue[AudioSegment] = Queue()
        self.sound_path = sound_path
        self.is_running = False
        self.play_thread = Thread(target=self._play_worker)

    def _mqtt_on_connect(self, client, userdata, flags, rc):
        self.mqtt.subscribe(self.vox_topic)
        self.mqtt.subscribe(self.tts_topic)

    def _mqtt_on_message(self, client, userdata, message: mqtt.MQTTMessage):
        if message.topic == self.vox_topic:
            full_path = os.path.join(self.sound_path, message.payload.decode())
            if os.path.isfile(full_path):
                self.play(full_path)
            else:
                self.say(f'File "{full_path}" was not found!')
        elif message.topic == self.tts_topic:
            self.say(message.payload.decode())

    def play(self, path: str):
        if os.path.isfile(path):
            self.sound_queue.put_nowait(AudioSegment.from_file(path))
            print(f'Sound "{path}" was added to queue.')

    def say(self, text: str):
        if text.strip():
            try:
                sound_file = io.BytesIO()
                gTTS(text).write_to_fp(sound_file)
                sound_file.seek(0)
                self.sound_queue.put_nowait(AudioSegment.from_file(sound_file))
                print(f'Text to speech "{text}" was added to queue.')
            except AssertionError:
                pass

    def _play_worker(self):
        while self.is_running:
            if not self.sound_queue.empty():
                play(self.sound_queue.get_nowait())
            else:
                time.sleep(0.01)

    def run(self):
        self.is_running = True
        self.play_thread.start()
        while self.is_running:
            self.mqtt.loop()
            time.sleep(0.01)

    def stop(self):
        self.is_running = False
        self.play_thread.join()


class MqttVoxClient(MqttVoxBase):
    def __init__(self, mqtt_address: Tuple[str, int] = MqttVoxBase.default_mqtt_address,
                 vox_topic: str = MqttVoxBase.default_vox_topic, tts_topic: str = MqttVoxBase.default_tts_topic):
        super().__init__(mqtt_address, vox_topic, tts_topic)
        self._connect()

        self.is_running = True
        self.loop_thread = Thread(target=self._loop_worker)
        self.loop_thread.start()

    def _loop_worker(self):
        while self.is_running:
            self.mqtt.loop()
            time.sleep(0.01)

    def close(self):
        self.is_running = False
        self.loop_thread.join()

    def say(self, text: str):
        self.mqtt.publish(
            self.tts_topic,
            text,
            1
        )

    def play(self, path: str):
        self.mqtt.publish(
            self.vox_topic,
            path,
            1
        )
