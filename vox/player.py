import io
import os
import time
from pydub import AudioSegment

os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = '1'

from pygame import mixer


class Player:
    def __init__(self):
        if not mixer.get_init():
            mixer.init()

    def __enter__(self) -> 'Player':
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def __del__(self):
        self.close()

    @staticmethod
    def play_async(segment: AudioSegment):
        sound_file = io.BytesIO()
        segment.export(sound_file, 'wav')
        sound_file.seek(0)
        mixer.Sound(sound_file).play()

    @classmethod
    def play_wait(cls):
        while mixer.get_busy():
            time.sleep(0.01)

    @classmethod
    def play(cls, segment: AudioSegment):
        cls.play_async(segment)
        cls.play_wait()

    @staticmethod
    def close():
        if mixer.get_busy():
            mixer.stop()
        if mixer.get_init():
            mixer.quit()


def play(segment: AudioSegment):
    with Player() as player:
        player.play(segment)


__all__ = [
    Player.__name__,
    AudioSegment.__name__,
    play.__name__
]
